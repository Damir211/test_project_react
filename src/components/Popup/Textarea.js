import React from "react";
import './Textarea.css'

function Textarea(props){
     const setValue = (event) => {
          props.setValue(event.target.value);
     }
     return(
         <textarea className="textarea" value={props.value} onChange={setValue} placeholder={props.placeholder} />
     )

}

export default Textarea