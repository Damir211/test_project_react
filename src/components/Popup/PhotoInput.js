import React from "react";
import './PhotoInput.css'

function PhotoInput(props){
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                props.setValue(e.target.result)
            };
            reader.readAsDataURL(input.files[0]);
        }else{
            props.setValue("")
        }
    }
    function imageSelect(event){
        readURL(event.target);
    }
    return(
        <div className="photo">
            <label className="photo__label" htmlFor="news-photo" style={{"backgroundImage": "url("+props.value+")"}}>
                {!props.value &&
                    <div>
                        <p>+</p>
                        <span>Выбрать фото</span>
                    </div>
                }
            </label>
            <input className="photo__input" id="news-photo" type="file" onChange={imageSelect} accept=".jpg"/>
        </div>
    )
}

export default PhotoInput