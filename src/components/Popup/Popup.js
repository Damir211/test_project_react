import React, {useState} from "react";
import './Popup.css'
import Input from "./Input";
import Button from "./Button"
import Textarea from "./Textarea";
import PhotoInput from "./PhotoInput";
import ManagerNewsItem from "./ManagerNewsItem";
import {connect} from 'react-redux'
import {createNewsProposed, createNews, login, deleteNewsProposed} from "../../redux/actions";

const Popup = (props) => {
    const [titleNews, setTitleNews] = useState('')
    const [textNews, setTextNews] = useState('')
    const [photoNews, setPhotoNews] = useState('')
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('');

    const sendNews = () => {
        if(titleNews.trim() && textNews.trim() && photoNews.trim()){
            const date = new Date();
            const dateString = date.getDate() + '.' + ((date.getMonth() + 1) < 10 ? ('0'+(date.getMonth() + 1)) : (date.getMonth() + 1)) + '.' + date.getFullYear();
            const news = {
                id: Date.now().toString(),
                text: textNews,
                title: titleNews,
                date: dateString,
                img: photoNews,
                userName: props.user.name,
            }
            props.createNewsProposed(news, props.setOpenpopup);
            setTitleNews('');
            setTextNews('');
            setPhotoNews('');
        }else{
            alert('Заполните все поля')
        }
    }

    const loginUser = () => {
        if(login.trim() && password.trim()){
            const userInfo = {
                login,
                password,
            }
            const response = props.login(userInfo);
            if(response.payload.name){
                setLogin('');
                setPassword('');
                props.setOpenpopup('');
            }
        }else{
            alert('Заполните поля!');
        }
    }

    const acceptNews = (news) => {
        props.createNews(news);
        props.deleteNewsProposed(news);
    }

    const deleteNews = (news) => {
        props.deleteNewsProposed(news);
    }

    return(
        (props.openpopup) && (
            <div className="popups">
                <div className={"popups__overlay"} onClick={props.setOpenpopup.bind('','')}/>
                {
                    props.openpopup === "popup-login" ?
                        <div className="popup popup-login">
                            <Input placeholder={"Логин"} value={login} setValue={setLogin} password={false}/>
                            <Input placeholder={"Пароль"} value={password} setValue={setPassword} password={true}/>
                            <Button text={"Войти"} clickHandler={loginUser}/>
                        </div>
                    : props.openpopup === "popup-addnews" ?
                        <div className="popup popup-addnews">
                            <PhotoInput value={photoNews} setValue={setPhotoNews} />
                            <Input placeholder={"Заголовок новости"} value={titleNews} setValue={setTitleNews} password={false}/>
                            <Textarea placeholder={"Текст новости"} value={textNews} setValue={setTextNews}/>
                            <Button text={"Добавить новость"} clickHandler={sendNews}/>
                        </div>
                    : props.openpopup === "popup-newsmanager" ?
                        <div className="popup popup-newsmanager">
                            <ManagerNewsItem acceptNews={acceptNews} deleteNews={deleteNews}/>
                        </div> : ''
                }
            </div>
        )
    )
}
const mapStateToProps = state => {
    return {
        user: state.user.user
    }
}

export default connect(mapStateToProps, {createNewsProposed, deleteNewsProposed, createNews, login})(Popup);