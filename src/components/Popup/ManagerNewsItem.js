import React from "react";
import NewsItem from "../NewsScreen/NewsItem";
import './ManagerNewsItem.css'
import {connect} from "react-redux";

function ManagerNewsItem(props){
    return(
        props.proposedNews.length > 0 ?
            props.proposedNews.map((item, index) => (
                <div className={"newsmanager-item"} key={item.id}>
                    <NewsItem
                        title={item.title}
                        text={item.text}
                        date={item.date}
                        img={item.img}
                    />
                    <div className={"manager-item-buttons"}>
                        <div className={"accept-news round-button"} onClick={props.acceptNews.bind('',item)}>✓</div>
                        <div className={"notaccept-news round-button"} onClick={props.deleteNews.bind('',item)}>x</div>
                    </div>
                </div>

            ))
        :
            <div className={"newsmanager-item"}>
                Нет новостей
            </div>
    )
}


const mapStateToProps = state => {
    return {
        proposedNews: state.news.proposedNews,
    }
}

export default connect(mapStateToProps, null)(ManagerNewsItem);