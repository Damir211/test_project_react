import React from "react";
import './Input.css'

function Input(props){
    const setValueInput = (event) => {
        props.setValue(event.target.value);
    }
    return(
        <input className="input" type={props.password ? 'password' : 'text'} value={props.value} onChange={setValueInput} placeholder={props.placeholder}/>
    )
}

export default Input
