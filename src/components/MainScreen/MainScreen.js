import React from 'react'
import './MainScreen.css'
import {connect} from 'react-redux'

function MainScreen(props){
    return(
        <div className='main'>
            <div className="main__container">
                <div className="wrapper">
                    Привет {props.user.name ? props.user.name : "Гость"}
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        user: state.user.user
    }
}

export default connect(mapStateToProps, null)(MainScreen);
