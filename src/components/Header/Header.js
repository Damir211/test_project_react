import React from 'react'
import './Header.css'
import {connect} from 'react-redux'
import {logout} from "../../redux/actions";
import {Link } from "react-router-dom";

function Header(props) {
    const openPopupLogin = () => {
        props.openPopup('popup-login')
    }
    return(
    	<header>
            <div className="wrapper">
                <Link  className={'header__item'} to="/">Главная</Link >
                <Link  className={'header__item'} to="/news">Новости</Link >
                {props.user.name ? <div className={'header__item'} onClick={props.logout}>Выход</div> : <div className={'header__item'} onClick={openPopupLogin}>Вход</div>}


            </div>
        </header>
    )
}
const mapStateToProps = state => {
    return {
        user: state.user.user
    }
}

export default connect(mapStateToProps, {logout})(Header);