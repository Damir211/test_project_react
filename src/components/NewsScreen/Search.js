import React from 'react';
import './Search.css'

function Search(props){
    const setSearchValue = (event) => {
        props.setValue(event.target.value)
    }
    return(
        <div className="search">
            <input type="text" placeholder="Поиск..." value={props.value} onChange={setSearchValue} className="search__input"/>
        </div>
    )
}

export default Search;