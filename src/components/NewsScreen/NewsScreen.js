import React, {useState} from 'react';
import Search from "./Search";
import Buttons from "./Buttons";
import NewsItem from "./NewsItem";
import './NewsScreen.css'
import {connect} from 'react-redux'



const NewsScreen = (props) => {
    const [search, setSearch] = useState('')
    return(
        <div className="news">
            <div className="wrapper">
                <Search value={search} setValue={setSearch}/>
                <Buttons openPopup={props.setOpenpopup} />
                <div className="news__container">
                    {props.proposedNews.filter(item => (item.userName === props.user.name)).filter(item =>  ((item.title.toLowerCase().indexOf(search.toLowerCase()) !== -1) ||
                                                (item.text.toLowerCase().indexOf(search.toLowerCase()) !== -1) ||
                                                (item.date.toLowerCase().indexOf(search.toLowerCase()) !== -1))
                    ).map(item => <NewsItem
                        title={item.title}
                        text={item.text}
                        date={'не подтверждено '+item.date}
                        img={item.img}
                        key={item.id}/>)
                    }
                    {props.news.filter(item => ((item.title.toLowerCase().indexOf(search.toLowerCase()) !== -1) ||
                                                (item.text.toLowerCase().indexOf(search.toLowerCase()) !== -1) ||
                                                (item.date.toLowerCase().indexOf(search.toLowerCase()) !== -1))
                    ).map(item => <NewsItem
                        title={item.title}
                        text={item.text}
                        date={item.date}
                        img={item.img}
                        key={item.id}/>)
                    }
                </div>
            </div>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        news: state.news.news,
        proposedNews: state.news.proposedNews,
        user: state.user.user,
    }
}

export default connect(mapStateToProps, null)(NewsScreen);