import React from "react";
import './NewsItem.css'

function NewsItem(props){
    return(
        <div className="news-item">
             <div className="news-item__img" style={{"backgroundImage": "url("+props.img+")"}}>

             </div>
            <div className="news-item__title">
                {props.title}
            </div>
            <div className="news-item__text">
                {props.text}
            </div>
            <div className="news-item__date">
                {props.date}
            </div>
        </div>
    )
}

export default NewsItem