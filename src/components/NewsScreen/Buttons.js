import React from "react";
import './Buttons.css'
import {connect} from "react-redux";

function Buttons(props){
    return(
        props.user.name === "admin" ?
            <div className="buttons">
                <div className="buttons__button" onClick={props.openPopup.bind('', 'popup-addnews')}>
                    Добавить новость
                </div>
                <div className="buttons__button" onClick={props.openPopup.bind('', 'popup-newsmanager')}>
                    Предложенные новости
                </div>
            </div>
           : props.user.name === "user" ?
            <div className="buttons">
                <div className="buttons__button" onClick={props.openPopup.bind('', 'popup-addnews')}>
                    Добавить новость
                </div>
            </div>
            : <div></div>
        
    )
}

const mapStateToProps = state => {
    return {
        user: state.user.user
    }
}

export default connect(mapStateToProps, null)(Buttons);
