import React, {useState} from 'react';
import MainScreen from './components/MainScreen/MainScreen'
import Header from './components/Header/Header'
import NewsScreen from "./components/NewsScreen/NewsScreen";
import Popup from "./components/Popup/Popup";
import {BrowserRouter, Route, Switch} from 'react-router-dom'
function App() {
    const [popup, openPopup] = useState('')
  return (
    <div className="App">
        <BrowserRouter>
            <Header openPopup={openPopup} />
            <Switch>
                <Route exact path="/">
                    <MainScreen />
                </Route>
                <Route path="/news">
                    <NewsScreen setOpenpopup={openPopup}/>
                </Route>
            </Switch>
      </BrowserRouter>
      <Popup openpopup={popup} setOpenpopup={openPopup}/>
    </div>
  );
}

export default App;
