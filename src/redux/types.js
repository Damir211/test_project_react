export const CREATE_NEWS = 'NEWS/CREATE_NEWS'
export const LOGIN_USER = 'USER/LOGIN_USER'
export const CREATE_NEWS_PROPOSED = 'NEWS/CREATE_NEWS_PROPOSED'
export const DELETE_NEWS_PROPOSED = 'NEWS/DELETE_NEWS_PROPOSED'