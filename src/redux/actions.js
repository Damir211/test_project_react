import {CREATE_NEWS, LOGIN_USER, CREATE_NEWS_PROPOSED, DELETE_NEWS_PROPOSED} from "./types";

export function createNews(post){
    return{
        type: CREATE_NEWS,
        payload: post
    }
}

export function deleteNewsProposed(post){
    return{
        type: DELETE_NEWS_PROPOSED,
        payload: post
    }
}

export function createNewsProposed(post, closePopup){
    closePopup('')
    return{
        type: CREATE_NEWS_PROPOSED,
        payload: post
    }
}

export function login(info){
    if(info.login === "admin"){
        if(info.password === "12345"){
            return{
                type: LOGIN_USER,
                payload: {
                    name: 'admin'
                }
            }
        }else{
            alert('Неправельный пароль');
            return{
                type: LOGIN_USER,
                payload: {}
            }
        }
    }else if(info.login === "user"){
        if(info.password === "12345"){
            return{
                type: LOGIN_USER,
                payload: {
                    name: 'user'
                }
            }
        }else{
            alert('Неправельный пароль');
            return{
                type: LOGIN_USER,
                payload: {}
            }
        }
    }else{
        alert('Данного пользователя не существует');
        return{
            type: LOGIN_USER,
            payload: {}
        }
    }

}
export function logout(){
    return{
        type: LOGIN_USER,
        payload: {}
    }
}