import {CREATE_NEWS, CREATE_NEWS_PROPOSED, DELETE_NEWS_PROPOSED} from "./types";

const initialState = {
    news: [{
        id: '1',
        title: 'Заголовок новости 1',
        text: 'Текст новости 1',
        date: '09.07.2020',
        img: 'https://moiarussia.ru/wp-content/uploads/2015/11/moscow-photo-02.jpg',
        userName: null,
    },{
        id: '2',
        title: 'Заголовок новости 2',
        text: 'Текст новости 2',
        date: '09.07.2020',
        img: 'https://moiarussia.ru/wp-content/uploads/2015/11/moscow-photo-02.jpg',
        userName: null,
    },{
        id: '3',
        title: 'Заголовок новости 3',
        text: 'Текст новости 3',
        date: '09.07.2020',
        img: 'https://moiarussia.ru/wp-content/uploads/2015/11/moscow-photo-02.jpg',
        userName: null,
    },{
        id: '4',
        title: 'Заголовок новости 4',
        text: 'Текст новости 4',
        date: '09.07.2020',
        img: 'https://moiarussia.ru/wp-content/uploads/2015/11/moscow-photo-02.jpg',
        userName: null,
    }],
    proposedNews: []

}

export const newsReducer = (state = initialState, action) => {
    switch(action.type){
        case CREATE_NEWS:
            return{...state, news: [action.payload, ...state.news]}
        case CREATE_NEWS_PROPOSED:
            return{...state, proposedNews: [action.payload, ...state.proposedNews]}
        case DELETE_NEWS_PROPOSED:
            return{...state, proposedNews: state.proposedNews.filter(item => (item.id !== action.payload.id))}
        default: return state
    }
}